<?php

namespace App\Controllers;

use Core\Controller;
use Core\View;
use App\Models\Equipement;
use App\Models\Chambre;
use App\Models\Adresse;

class AnnoncesController extends Controller
{

    public function body(?array $view_data = null): void
    {

        $view = new View( 'annonce-post' );

		

		$view->render( $view_data );

    }
    public function addAnnonce(): void{
        /**
         * Un annonce est composée de 
         *  I. CREATION D'OBJ:
         *      -A CHAMBRE
         *      -B ADRESSE
         *      -C EQUIPEMENT 
         * II. PASSAGE DE L'OBJ DANS SON REPOSITORY
         */

        // I.
        // A- la chambre 
        $chambre = new Chambre();
        $chambre->label = $_POST['label'];
        $chambre->annonceur_id =    $_SESSION['isConnected']->id;
        $chambre->prix_n = $_POST['prix_n'];
        $chambre->type = $_POST['type'];
        $chambre->description = $_POST['description'];
        $chambre->couchage = $_POST['couchage'];
        $chambre->taille = $_POST['taille'];
        $chambre->date_debut = $_POST['date_debut'];
        $chambre->date_fin = $_POST['date_fin'];
        // traitement de l'image:
        
        $img= $_FILES['image_chambre'];
        if(isset($img)){
            // mettre limg dans un dossier et enregistrer le chemin.
            $extension = pathinfo($img['name'],PATHINFO_EXTENSION);
            $titlePath= strtolower(preg_replace('#\s#', '', $_POST['label']));
            $target = 'images/'.$titlePath.'.'.$extension;
            move_uploaded_file( $img['tmp_name'],$target);
    
            $chambre->image = $target;
        }

        $newChambre = $this->rm->getChambreRepo()->create($chambre);

        // B- ADRESSE
        $adresse = new Adresse();
        $adresse->chambre_id= $newChambre->id;
        $adresse->pays= $_POST['pays'];
        $adresse->code_postal= $_POST['code_postal'];
        $adresse->ville= $_POST['ville'];
        $adresse->voie= $_POST['voie'];
        $_POST['numero']= isset($_POST['numero'])? $_POST['numero']: null;
        $_POST['numero']= is_numeric($_POST['numero'])? $_POST['numero']: null;
        $adresse->numero= $_POST['numero'];

        $newAdresse = $this->rm->getAdresseRepo()->create($adresse);

        //  C-EQUIPEMENTS
        /*
            RECUPERER LES OBJ EQUIPEMENT:
                - il y en a 6, 
                nommées "equipement_i"
                si ils sont checked-> alors
                ils sont postés -> alors
                on les met dans un tableau
        */
        
        $equipement= [];
        for($i = 0; $i< 5;){
            $i++;
            $equipement_name = 'equipement_'.$i;
            if(isset($_POST[$equipement_name])){
                $equipement[]= $_POST[$equipement_name];
            }

        }
        foreach($equipement as $value){
            $equip = new Equipement();
            $equip->chambre_id=  $newChambre->id;
            $equip->materiel=$value;
            $newEquipement = $this->rm->getEquipementRepo()->create($equip);

        }
        
        // ET POUR FINIR REDIRECTION
        $location = "Location: ".MYSITE_URI.ROOT_;
		header($location);

	}


}