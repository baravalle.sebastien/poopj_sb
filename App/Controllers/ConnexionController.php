<?php

namespace App\Controllers;

use Core\Controller;
use Core\View;
use App\Models\User;
class ConnexionController extends Controller
{
    public function body(): void
    {

        $view = new View( 'connexion' );

		$view_data = [
		];

		$view->render( $view_data );

	}
	public function addUser(): void{
		$user = new User();
		$user->login = $_POST['loginInscription']; 
		$user->password = hash('sha256',$_POST['passwordInscription']);
		$user->role = $_POST['role'];


		$newUser = $this->rm->getUserRepo()->create($user);
		$_SESSION['isConnected']=$newUser;
	}
	public function checkUser(): void{

		$user = new User();
		$user->login = $_POST['loginConnexion']; 
		$user->password = hash('sha256',$_POST['passwordConnexion']);
		
		$connectedUser = $this->rm->getUserRepo()->connect($user);
		$_SESSION['isConnected']=$connectedUser;
		
	}
	public function deconnect(): void{
		$_SESSION = array();

		if(ini_get("session.use_cookies")){
			$params = session_get_cookie_params();
			setcookie(session_name(), '', time() -42000,
			$params["path"],$params["domain"],
			$params["secure"],$params["httponly"],
			);
		}
		session_destroy();
		$location = "Location: ".MYSITE_URI.ROOT_;
		header($location);
		exit;
	}


}
