<?php

namespace App\Controllers;

use Core\Controller;
use Core\View;
use App\Models\Reservation;
use App\Models\Chambre;
use DateTime;

class ReservationController extends Controller
{
    
    public function body(?array $view_data = null): void
    {

        $view = new View( 'reservation-post' );

		

		$view->render( $view_data );

    }

   public function reserver(array $args){
        $chambre_id =$args['chambre_id'];
        $user_id=$args['user_id'];
        $newChambre = $this->rm->getChambreRepo()->findID($chambre_id);
        $reservation = new Reservation;
        $reservation->chambre_id = $newChambre->id;
        $reservation->client_id = $user_id;
        $reservation->date_debut = $_POST['date_debut'];
        $reservation->date_fin = $_POST['date_fin'];

        // CALCUL PRIX * NOMBRE DE JOURS
        $date_debut =date_create($_POST['date_debut']); 
        $date_fin =date_create($_POST['date_fin']); 
        $decalage = date_diff($date_debut, $date_fin);
        $jours= $decalage->days;        echo"<br><br><br>";
        $reservation->prix_t= $newChambre->prix_n * $jours;

        // CONDITION DE DISPONIBILITÉ:
        $date_appart_dispo = strtotime($newChambre->date_debut);
        $date_fin_dispo = strtotime($newChambre->date_fin);
        $date_fin= strtotime($_POST['date_fin']);
        $date_debut= strtotime($_POST['date_debut']);

        if($date_debut<=$date_appart_dispo || $date_fin >= $date_fin_dispo || $date_debut > $date_fin){
            echo "date invalide";
            return false;
        }

        $newReservation = $this->rm->getReservationRepo()->create($reservation);
		$location = "Location: ".MYSITE_URI.ROOT_;
		header($location);

        var_dump($newReservation);
   }
}
