<?php

namespace App\Controllers;

use Core\Controller;
use Core\View;
use App\Models\Equipement;
use App\Models\Chambre;
use App\Models\Adresse;
use App\Repositories\RepositoryManager;

class SearchController extends Controller
{
    public function search($arg){
        if(is_string($arg)){
            $retourSearch = $this->rm->getChambreRepo()->searchString($arg);
            $tab= [];
            foreach($retourSearch as $key => $value){
                // VOIR LA CLASSE DE RETOUR: ALLER CHERCHER ALL ROOM INFO D'APRE CLASS
                if($key == 'App\Models\User'){

                    $tab[]= $this->rm->getAllRoomInfoRepo()->getAllRoomInfo($value[0]->id);

                }
                if($key == 'App\Models\Chambre'){
                    $tab[]= $this->rm->getAllRoomInfoRepo()->getAllRoomInfo(null,$value[0]->id);

                }
                if($key == 'App\Models\Adresse'){
                    foreach($value as $adresse){
                        $tab[]= $this->rm->getAllRoomInfoRepo()->getAllRoomInfo(null,$adresse->chambre_id);

                    }

                }
                if($key=='App\Models\Equipement'){
                    foreach($value as $equipement){
                        $tab[]= $this->rm->getAllRoomInfoRepo()->getAllRoomInfo(null,$equipement->chambre_id);
 
                    }
                }

            }
            echo"<br><br> <h4>tb</h4>".var_dump($tab)."<br><br>";

            return $tab;

        }
    }
}