<?php

namespace App\Models;

use Core\Model;
class Adresse extends Model
{
    public int $chambre_id;
    public string $pays;
    public int $code_postal;
    public string $ville;
    public string $voie;
    public ?int $numero;



}