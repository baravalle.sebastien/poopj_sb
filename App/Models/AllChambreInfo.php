<?php

namespace App\Models;

use Core\Model;
class AllChambreInfo extends Model
{
    public Chambre $chambre;
    public Adresse $adresse;
    public ?array $equipements;
    public ?Reservation $reservation;
    public ?User $annonceur;
    public ?User $client;
    public function __construct( array $data = [] )
	{
        //SEPARER LE TABLEAU POUR CREER CLASSES
        $this->chambre= $data['Chambre'];
        $this->adresse = $data['Adresse'];
        $this->equipements = $data['Equipement'];
    }


    
}