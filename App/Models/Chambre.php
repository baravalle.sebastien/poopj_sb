<?php

namespace App\Models;

use Core\Model;
class Chambre extends Model
{
    public string $label;
    public int $annonceur_id;
    public int $prix_n;
    public int $type;
    public ?string $description;
    public int $couchage;
    public int $taille;
    public ?string $image;
    public $date_debut;
    public $date_fin;

    
}