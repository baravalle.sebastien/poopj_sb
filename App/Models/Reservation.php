<?php

namespace App\Models;

use Core\Model;
class Reservation extends Model
{
    public int $chambre_id;
    public int $client_id;
    public $date_debut;
    public $date_fin;
    public int $prix_t;
}