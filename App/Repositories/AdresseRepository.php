<?php

namespace App\Repositories;

use Core\Repository;
use App\Models\Adresse;

class AdresseRepository extends Repository
{
    public function getTable(): string

    {
        return 'adresses';
    }

   
    public function findAll(): array
    {
        return $this->readAll(Adresse::class);
    }

    public function findID(int $id): ?Adresse
    {
        return $this->readById($id , Adresse::class);
    }
}
