<?php

namespace App\Repositories;

use Core\Repository;
use App\Models\Chambre;
use \PDO;
use App\Models\Equipement;
use App\Models\Adresse;
use App\Models\AllChambreInfo;
class AllRoomInfoRepository extends Repository
{
    public function getTable(): string

    {
        return 'chambres';
    }

   
    public function getAllRoomInfo(?int $user_id = null, ?int $room_id= null){
        /*
        GET ALL ROOM INFO RENVOI UN TABLEAU 
            D'OBJET ALL CHAMBRE INFO: composée d'objet

                1- si un id est passé en parametre
                    A- il va chercher les chambres appartenant a cet user id
                    B-  sinon il prend toute les chambres
                2 pour chaque chambre il :
                    -la transforme en objet -> la met dans tableau
                    puis cherche:
                    A- l'adresse Corespondante
                        -la transforme en objet -> le met dans tableau
                    B-  Les Equipement corespondant
                        -les transforme en tableau d'objet -> les met dans tableau
                    C- transforme le tableau en OBJ ALLROOMINFO -> le met dans tableau
                
        */

        $tableau = [];

        // 1- A
        if ($user_id != null){
            $query = 'SELECT * FROM chambres WHERE annonceur_id= :annonceur_id';
            $sth= $this->db_cnx->prepare( $query );
            $sth->execute(
                array(
                    'annonceur_id'=> $user_id
                )
            ); 
    
        }
        else if( $room_id !=  null ){
            $query = 'SELECT * FROM chambres WHERE id= :room_id';
            $sth= $this->db_cnx->prepare( $query );
            $sth->execute(
                array(
                    'room_id'=> $room_id
                )
            ); 

        }
        else
        // 1- B
        {
            $query = 'SELECT * FROM chambres ';
            $sth= $this->db_cnx->prepare( $query );
            $sth->execute(); 
    
        }
        if($sth->errorCode() !== PDO::ERR_NONE){
            return null; 
        }
        //  2.
        while($chambre = $sth->fetch()){

            $obj_complete = [];
            $obj_chambre = new Chambre($chambre);
            $obj_complete['Chambre']= $obj_chambre; 

        // 2- A ADRESSE
            $query = 'SELECT * FROM adresses WHERE chambre_id= :chambre_id';
            $sth2= $this->db_cnx->prepare( $query );
            $sth2->execute(
                array(
                    'chambre_id'=> $obj_chambre->id
                )
            ); 
            $obj_adresse = $sth2->fetch();
            $obj_adresse = new Adresse($obj_adresse);
            $obj_complete['Adresse']= $obj_adresse;
            
        // 2- B EQUIPEMENT
            $query = 'SELECT * FROM equipements WHERE chambre_id= :chambre_id';
            $sth3= $this->db_cnx->prepare( $query );
            $sth3->execute(
                array(
                    'chambre_id'=> $obj_chambre->id
                )
            );
            $tableauObjtEquipement = [];
            while($equipement = $sth3->fetch())
            {   
                $obj_equipement = new Equipement($equipement);
                $tableauObjtEquipement[]= $obj_equipement;
            }
            $obj_complete['Equipement']= $tableauObjtEquipement; 
            $allInfo =  new AllChambreInfo($obj_complete); 
            $rm= RepositoryManager::getRm();

            $obj_annonceur =  $rm->getUserRepo()->findID($allInfo->chambre->annonceur_id);
            $allInfo->annonceur= $obj_annonceur;
            
            $tableau[] = $allInfo;


        }
        return $tableau;

    }
}