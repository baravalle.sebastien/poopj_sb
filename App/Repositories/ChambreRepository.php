<?php

namespace App\Repositories;

use Core\Repository;
use App\Models\Chambre;
use \PDO;
use App\Models\Equipement;
use App\Models\Adresse;

class ChambreRepository extends Repository
{
    public function getTable(): string

    {
        return 'chambres';
    }

   
    public function findAll(): array
    {
        return $this->readAll(Chambre::class);
    }

    public function findID(int $id): ?Chambre
    {
        return $this->readById($id , Chambre::class);
    }
}
