<?php

namespace App\Repositories;

use Core\Database;

class RepositoryManager
{   

    private static ?self $instance = null;

    /*
        METHODES POUR RECUPERER REPO
    */
    private AdresseRepository $adresse_repo;
    public function getAdresseRepo(): AdresseRepository
    {
        return $this->adresse_repo;
    }

    private ChambreRepository $chambre_repo;
    public function getChambreRepo(): ChambreRepository
    {
        return $this->chambre_repo;
    }

    private ReservationRepository $reservation_repo;
    public function getReservationRepo(): ReservationRepository
    {
        return $this->reservation_repo;
    }
    private UserRepository $user_repo;
    public function getUserRepo(): UserRepository
    {
        return $this->user_repo;
    }

    private EquipementRepository $equipement_repo;
    public function getEquipementRepo(): EquipementRepository
    {
        return $this->equipement_repo;
    }

    private AllRoomInfoRepository $allRoomInfo_repo;
    public function getAllRoomInfoRepo(): AllRoomInfoRepository
    {
        return $this->allRoomInfo_repo;
    }



    public static function getRm(): self
    {
        if(is_null( self::$instance) ) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __construct()
    {
        $pdo = Database::get();

        $this->chambre_repo = new ChambreRepository( $pdo );
        $this->adresse_repo = new AdresseRepository( $pdo );
        $this->reservation_repo = new ReservationRepository( $pdo );
        $this->user_repo = new UserRepository( $pdo );
        $this->equipement_repo = new EquipementRepository( $pdo );
        $this->allRoomInfo_repo = new AllRoomInfoRepository( $pdo );

    }

    private function __clone() {}

    private function __wakeup() {}
}