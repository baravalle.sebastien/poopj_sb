<?php

namespace App\Repositories;

use Core\Repository;
use App\Models\Reservation;

class ReservationRepository extends Repository
{
    public function getTable(): string

    {
        return 'reservations';
    }

   
    public function findAll(): array
    {
        return $this->readAll(Reservation::class);
    }

    public function findID(int $id): ?Reservation
    {
        return $this->readById($id , Reservation::class);
    }
    public function reservationByUser(array $arg){
        if(isset($arg['client_id'])){
            $query = 'SELECT * FROM reservations WHERE client_id= :client_id';
            $sth= $this->db_cnx->prepare( $query );
            $sth->execute(
                array(
                    'client_id'=> $arg['client_id']
                )
            ); 

        }
        if(isset($arg['annonceur_id'])){
            $query = 'SELECT reservations.date_debut, reservations.date_fin, reservations.id, reservations.chambre_id, reservations.client_id, reservations.prix_t FROM reservations LEFT JOIN chambres ON reservations.chambre_id= chambres.id WHERE chambres.annonceur_id = :annonceur_id';
            $sth= $this->db_cnx->prepare( $query );
            $sth->execute(
                array(
                    'annonceur_id'=> $arg['annonceur_id']
                )
            ); 
            
        }
        $tableau= [];
        while($reservation = $sth->fetch()){
            $obj_reservation = new Reservation($reservation);
            $rm= RepositoryManager::getRm();
            $allRoomInfo= $rm->getAllRoomInfoRepo()->getAllRoomInfo(null, $obj_reservation->chambre_id);
            $allRoomInfo[0]->reservation = $obj_reservation;
            $allRoomInfo[0]->client =  $rm->getUserRepo()->findID($obj_reservation->client_id);

            $tableau[]= $allRoomInfo[0];
        }
        return $tableau;
    }
}
