<?php

namespace App\Repositories;

use Core\Repository;
use App\Models\User;
use \PDO;

class UserRepository extends Repository
{
    public function getTable(): string

    {
        return 'users';
    }

   
    public function findAll(): array
    {
        return $this->readAll(User::class);
    }

    public function findID(int $id): ?User
    {
        return $this->readById($id , User::class);
    }
    public function connect(User $user) : ?User{

        $query = sprintf(
            'SELECT * FROM %s WHERE login= :login AND password = :password',
            $this->getTable()
        );
        $sth= $this->db_cnx->prepare( $query );
        $sth->execute(
            array(
                'login'=> $user->login,
                'password'=> $user->password
            )
        ); 
        $connectedUser= $sth->fetch();
        if($connectedUser=== null || $connectedUser=== false){

			$location = "Location: ".MYSITE_URI.ROOT_;
			header($location);
	
		}

        $connectedUser = is_null($connectedUser) ? null :  new User($connectedUser);
        return $connectedUser;

    }
}
