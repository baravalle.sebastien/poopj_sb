<?php

namespace App\Utilities;

class IsConnected
{
    public  function Ask(): bool
    {
        $isConnected = isset($_SESSION['isConnected']);
        return $isConnected;
    }

}
