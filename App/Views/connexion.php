<body class="body-no-connect">
<div class="title-no-connect">
    <h1>tdPoo.com</h1>
</div>
    
<main>

<button type="button" id='switchForm'><span id="icone"><i class="fas fa-exchange-alt"></i></span>
    <span id='texteIcone'>
        je veux m'inscrire
    
    </span>
</button>

    <form id='formulaireConnexion' name='formulaire' action="index.php?action=connexion" method="post">
        <h2> Connectez vous : </h2>
        <span class="notificationErreur">
            <input id="login_co" name="loginConnexion" type="text" placeholder="login">

        </span>

        <span class="notificationErreur">
            <input id="password_co" name="passwordConnexion" type="password" placeholder="mot de passe ">

        </span>
        <button  id="submit_co" type="submit"> <i class="fas fa-house-user"></i></i>Connexion!</button>

    </form> 

    <form id='formulaireInscription' action="index.php?action=inscription" method="post">
        <h2> Inscrivez vous : </h2>
        <span class="notificationErreur">
            <input name="loginInscription" type="text" placeholder="login">

        </span>

        <span class="notificationErreur">
            <input name="passwordInscription" type="password" placeholder="mot de passe ">

        </span>
        <span class="notificationErreur">
            <input name="passwordInscription" type="password" placeholder="mot de passe ">

        

<div>
  <input type="radio" id="client" name="role" value="client"
         checked>
  <label for="client">client</label>
</div>

<div>
  <input type="radio" id="annonceur" name="role" value="annonceur">
  <label for="annonceur">annonceur</label>
</div>

        <button id="submitInscription" type="submit"><i class="fas fa-sign-in-alt"></i>inscription</button>

    </form> 

</main>
<script>
/* II. FONCTION AFFICHAGE ERREUR*/
function notifieErreur(value, message, id ){
    let supprNotif = document.querySelector('#'+id);
    if(supprNotif){
        supprNotif.remove();
    }

    let messageErreur = document.createElement("div");
    messageErreur.setAttribute('id', id);
    messageErreur.setAttribute('class', 'erreursNotif');

    messageErreur.innerHTML = message;


    let parent = value.parentNode;
    parent.appendChild(messageErreur);

}

let trigger_connexion_verif = document.querySelector('#submit_co');

trigger_connexion_verif.addEventListener('click', function(e){

    let input_login = document.querySelector('#login_co');
    let input_password = document.querySelector('#password_co');
    console.log(input_login.value);
    if(/^[a-zA-Zèïêé]+[0-9]?/.test(input_login.value)&& /[\d\w\W]{3,13}/.test(input_login.value) && input_login.value != null){

    }else{
        e.preventDefault();
        notifieErreur(input_login, "&#10060;nom invalide", 'notifNom');

    }
    if(/[\d\w\W\D\s\S]{3,12}/.test(input_password.value)){

    }else{
        e.preventDefault();
        notifieErreur( input_password, "&#10060;mdp invalide", 'notifNom');

    }

});



/*
        DEUX FORMULAIRE SONT PRESENT SUR LA PAGE
        un de connexion un d'inscription
        grace a un btn on fait apparaitre le form de  
        connexion ou d'inscription.
            tour a à tour on display l'un ou l'autre 
    */
    let montreFormulaireInscription = false;    // booleen qui indique le form visible

let switchForm = document.querySelector('#switchForm');

let formulaireConnexion = document.querySelector('#formulaireConnexion');
let formulaireInscription = document.querySelector('#formulaireInscription');
let login= document.querySelector('#login');
formulaireInscription.style.display = 'none';

switchForm.addEventListener('click', function(){
    if (montreFormulaireInscription){
        formulaireConnexion.style.display = 'block';
        formulaireInscription.style.display = 'none';
        document.querySelector('#texteIcone').innerHTML = 'je veux m\'inscrire';
        montreFormulaireInscription = false;
        setTimeout(function(){
            document.querySelector('#loginConnexion').focus();

        },1000);
    }else{
        formulaireInscription.style.display = 'block';
        formulaireConnexion.style.display = 'none';
        document.querySelector('#texteIcone').innerHTML = 'je veux me connecter';
        montreFormulaireInscription = true;
        setTimeout(function(){

            document.querySelector('#loginInscription').focus();
        },1000);

    }
});

/*
    MASQUE
*/

</script>

</body>

</html>