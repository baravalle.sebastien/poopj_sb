<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$title_head ?></title>
    <link href="https://fonts.googleapis.com/css2?family=Kumbh+Sans&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="css/style.css">
    <?php 
        if(isset($cssTab)){
            
            foreach($cssTab as $css){
                echo $css;
            }
        }
    ?>
    <script src="https://kit.fontawesome.com/57535fa6f1.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Kumbh+Sans&display=swap" rel="stylesheet">
</head>

