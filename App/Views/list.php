	
	
	<?php
	
	if( count($list) > 0 ): ?>
		<ul class="liste">
<?php 
	$i=0;
	foreach($list as $item){
		if(is_array($item)){
			$item= $item[0];
		}
		$i++;
/*
**	**	**	**	**	**	**	**	**	
**	AFFICHAGE D'OBJT	
**	**	**	**	**	**	**	**	**	
	A- SELON LE NOM DE LA CLASSE
		ex:
		-Chambre
		-Allinfo
	B- AFFICHAGE CORRESPONDANT
*/

		

	// A-
	$className= get_class($item);

	// B-
	if($className=='App\Models\Chambre'){
?>
<li><strong><?php echo $item->label ?></strong> - <?php echo $item->description ?></li>
<?php		
	}

	// B-
	if($className=='App\Models\Equipement'){
?>

<li><strong><?php echo $item->materiel ?></strong> - </li>
<?php		
	}

	// B-
	if($className=='App\Models\AllChambreInfo'){
		?>
		
<li class="chambre">
	<div class="img-container">
		<canvas  id="canevas-img<?=$i?>" class="canevas-img" height="100" width="600"> </canvas>
		<script >
			var canevas = document.getElementById('canevas-img<?=$i?>');
			var ctx = canevas.getContext('2d');
			ctx.beginPath();
			ctx.moveTo(0,0);
			ctx.lineTo(600,0);
			ctx.lineTo(600,50);

			ctx.quadraticCurveTo(600,0,0,0);
			ctx.fillStyle = '#fefefe';
			ctx.fill();

		</script>


		<img src="<?= $item->chambre->image?>" alt="">
	</div>

	<h2><?=$item->chambre->label?></h2>

	<div class="renseignement">

		<span class="description"><?=$item->chambre->description?></span>
		
		<span class="equipement"> <?php 
			foreach($item->equipements as $equipement){
				echo"<span>".$equipement->materiel."</span> ";
			}
		?>
		</span>

		<span class="prix"><?= $item->chambre->prix_n?> € / nuit</span>

		<span class="adresse"> adresse : <?= $item->adresse->numero.' '.$item->adresse->voie.' '.$item->adresse->ville.' '.$item->adresse->code_postal ?></span>


		<?php

			if(isset($item->reservation))
			{
			?>
				<h3>Chambre reservée</h3>
				<span>La chambre de <?= $item->annonceur->login?> est louée par <?=$item->client->login ?> </span>
				<span>Du <?php echo date('Y-m-d ', strtotime($item->reservation->date_debut));?>  au  <?php echo date('Y-m-d ', strtotime($item->reservation->date_fin));?></span>
				<span> Pour un prix total de <strong><?= $item->reservation->prix_t?></strong></span>
			<?php
			}
		?>

	<?php 
			if($_SESSION['isConnected']->role =="client"){
		?>

				<form method="post" action="index.php?addreservation">
					<input  class="btn-reserver" type="submit"  value="reserver" >
					<input type="hidden" name="add_reservation" value="<?= $item->chambre->id?>"  >
				</form>
		<?php
			}
		
		?>
	<canvas  id="canevas-img-bas<?=$i?>" class="canevas-img-bas" height="100" width="600"> </canvas>
	<script >
		var canevas = document.getElementById('canevas-img-bas<?=$i?>');
		var ctx = canevas.getContext('2d');
		ctx.beginPath();
		ctx.moveTo(0,0);
		ctx.quadraticCurveTo(0,100,600,50);
		ctx.lineTo(600,100);
		ctx.lineTo(0,100);
		ctx.lineTo(0,0);

		ctx.fillStyle = '#fefefe';
		ctx.fill();

	</script>


	</div>
</li>
<?php		
	}
?>		

				
			
			


<?php
 	} 
?>
		</ul>
	<?php endif; ?>
</body>
</html>
