<!-- 
LE MENU 
    un parametre page 
    rend ce menu dynamique,
    il affiche les liens apart si 
    page est déja celle du lien
    au quel cas le lien est vers le menu

-->
<body class="body-connected">
       
<nav>
<div class="title-connected">
    <h1>tdPoo.com</h1>
    <form method="POST"action="index.php?search">

        <input type="text" name="search_field" id="search_field" class="display field_search">
    </form>


        <i class="fas fa-search btn-search"></i>
    <script>
        let btn_search= document.querySelector('.btn-search');
        let search_field= document.querySelector('#search_field');
        let visibleField = false; 

        btn_search.addEventListener('click', function(e){
            if(visibleField){
                search_field.classList.add('display');
                visibleField = false;
            }else{
                visibleField= true;
                search_field.classList.remove('display');
            }

        });
    </script>
</div>
<div class="slogan"><h2>
Air bnb l'a fait
</h2>
<span> Lorem ipsum dolor Ea aut assumenda, eum odio labore nisi ad, illo cum, perferendis cumque autem.</span>
</div>
    <ul class="menu">
        <li>
            <a href="index.php?deconnect">
                deconnexion <?= htmlentities($user_name) ?>
            </a>
        </li>

<?php
        if($user_role === 'annonceur'){
            $page = isset($page)? $page : null;
?>          

            <li>
<?php
                if($page === 'addAnnonces'){
?>
                <a href="index.php">
                    Accueil
                </a>


<?php        
                }else{
?>
                <a href="index.php?addannonces">
                    Poster
                </a>


<?php
                }
?>
            </li>
            <li>
<?php
                if($page === 'myReservations'){
?>
                <a href="index.php">
                    Accueil
                </a>


<?php        
                }else{
?>
                <a href="index.php?myreservations">
                    Réservations
                </a>


<?php
                }
?>
            </li>
<?php
        }
?>
<?php
        if($user_role === 'client'){
            $page = isset($page)? $page : null;
?>          

            <li>
<?php
                if($page === 'myReservations'){
?>
                <a href="index.php">
                    Accueil
                </a>


<?php        
                }else{
?>
                <a href="index.php?myreservations">
                    Reservations
                </a>


<?php
                }
?>
            </li>
<?php
        }
?>





    </ul>
    <canvas class="frame" id="canevas" height="100" width="600">< /canvas>
    <script >
        var canevas = document.getElementById('canevas');
        var ctx = canevas.getContext('2d');
        ctx.beginPath();
        ctx.moveTo(0,0);
        ctx.quadraticCurveTo(150,0,300,50);
        ctx.quadraticCurveTo(450,0,600,0);
        ctx.lineTo(600,100);
        ctx.lineTo(0,100);
        ctx.lineTo(0,0);
        ctx.fillStyle = '#fefefe';
        ctx.fill();


    </script>
</nav>