<?php

namespace Core;
use App\Repositories\RepositoryManager;
use App\Controllers\SearchController;

abstract class Controller
{
    protected RepositoryManager $rm;

	public function __construct()
	{
		$this->rm = RepositoryManager::getRm();
	}

	public function head( ?array $view_data = null): void
	{
		$view = new View( 'head' );

		$view->render( $view_data );
	}

	public function showMenu(?array $view_data = null): void
	{
		$view  = new View( 'menu' );


		$view->render( $view_data );
	}

	// LISTE PERMET D'ALLER CHERCHER LISTE DANS REPO ET AFFICHER
	public function List(?array $view_data = null): void
    {

		$view = new View( 'list' );
		/**
		 * LIST peux:
		 * 1. prendre un tableau d'objet qui sera affiché dans view list
		 * 2. prendre une requete a executer dans repository demandé
		 * 		B- avec ou sans argument (ex: id) pour la requette
		 * 
		*/
		// si un argu repo est passé il  va chercher 
		// dynamiquement le repository puis sa fonction.

		if(isset( $view_data['repo'])){
			$getRepo = $view_data['repo'];
			$request = $view_data['request'];
			if (isset($view_data['arg'])){
				$arg= $view_data['arg'];
				$view_data['list']= $this->rm->$getRepo()->$request($arg);

			}else{

				$view_data['list']= $this->rm->$getRepo()->$request();
			}
		}
		if(isset($view_data['ctrl'])){
			$ctrl = $view_data['ctrl'];
			$request = $view_data['request'];
			if (isset($view_data['arg'])){
				$arg= $view_data['arg'];
				$obj= new $ctrl;
				$obj= $obj->$request($arg);
				$view_data['list']= $obj;

			}

		}
		$view->render( $view_data );

	}



}
