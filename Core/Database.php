<?php
namespace Core;
use \PDO;

class Database
{
    private static ?PDO $instance=null;

    public static function get() :PDO
    {   
        if( is_null(self::$instance)){
           self::$instance = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8',DB_USER,DB_PASS);
        }
        return self::$instance;
    }
    private function __construct()
    {

    }
    private function __clone(){

    }
    private function __wakeup(){

    }
}