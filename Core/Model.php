<?php

namespace Core;

abstract class Model
{
	public int $id;

	public function __construct( array $data = [] )
	{
		$this->hydrate( $data );
	}

	private function hydrate( array $data = [] ): void
	{
		foreach( $data as $column => $value ) {
			// get_called_class() recupère le nom de la classe enfant qui a été instanciée
			if( property_exists( get_called_class(), $column ) ) {
				$this->$column = $value;
			}
		}
	}
}