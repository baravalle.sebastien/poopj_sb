<?php
namespace Core;

use \PDO;
use App\Models\Adresse;
use App\Models\Chambre;
use App\Models\Equipement;
use App\Models\User;

abstract class Repository
{
    protected PDO $db_cnx;

    public abstract function getTable(): string;

    public function __construct(PDO $pdo)
    {   
        $this->db_cnx = $pdo;
    }

    private function instancie($request, $arg, $cnx, $className) {
        $sth =$cnx->prepare($request);
        $sth->execute(array('arg'=>$arg));

        $obj_tab=[];
        while($raw = $sth->fetch()){
            if(!is_bool($raw)){
                $obj = new $className($raw);
                $obj_tab[]= $obj;
            }

            
            
        }
        if(!empty($obj_tab)){
            
            return $obj_tab;
        }
    }
    public function searchString($arg){
        $results = [];
        $request="SELECT * FROM  chambres WHERE label=:arg";
        $filtre = $this->instancie($request,$arg, $this->db_cnx, 'App\Models\Chambre');
        if(!is_null($filtre)){$results['App\Models\Chambre']= $filtre;}
        $request2="SELECT * FROM  adresses WHERE pays = :arg OR ville=:arg";
        $filtre = $this->instancie($request2,$arg, $this->db_cnx , 'App\Models\Adresse');
        if(!is_null($filtre)){$results['App\Models\Adresse']= $filtre;}

        $request3="SELECT * FROM  equipements WHERE materiel = :arg";
        $filtre = $this->instancie($request3,$arg, $this->db_cnx, 'App\Models\Equipement');
        if(!is_null($filtre)){$results['App\Models\Equipement']= $filtre;}

        $request4="SELECT * FROM  users WHERE login = :arg";
        $filtre = $this->instancie($request4,$arg, $this->db_cnx, 'App\Models\User');
        if(!is_null($filtre)){$results['App\Models\User']= $filtre;}

        return $results;

    }
    public function create(Model $modele) : ?Model
    {
        $table = $this->getTable();
        $object = get_object_vars($modele);
        $column = array_keys($object);
        $req_prep="INSERT INTO $table (".implode(', ',$column).") VALUES( :".implode(', :',$column)." );" ;
        $sth =$this->db_cnx->prepare($req_prep);
        $sth->execute($object);

        // par ce moyen on sait si insertion 
        if($sth-> errorCode()===PDO::ERR_NONE){
            $modele->id = $this->db_cnx->lastInsertId();

            return $modele;
        }
            return null;

    }
    protected function readAll($className): array
    {        
        $table = $this->getTable();

        $query= "SELECT * FROM $table ;";
        $sth= $this->db_cnx->query( $query );
        $arr_objects= [];        
        
        // par ce moyen on sait si insertion 
        if($sth-> errorCode()!==PDO::ERR_NONE){
            return $arr_objects;
        }

        while($row = $sth->fetch()){
            $obj_row = new $className($row);
            $arr_objects[]= $obj_row;
        }
        return $arr_objects;
    }
    public function readById( int $id, string $className): ?Model
    {   
        $query = sprintf(
            'SELECT * FROM %s WHERE id= :id',
            $this->getTable()
        );
        $sth= $this->db_cnx->prepare( $query );
        $sth->execute(
            array(
                'id'=>$id
            )
        ); 
        if($sth->errorCode() !== PDO::ERR_NONE){
            return null;
        }
        
        while($row = $sth->fetch())
        {

                $obj_row = new $className($row);
                return $obj_row;
           
        }
        return null;
    }
    public function update(Model $modele){
        $tableName = $this->getTable();
        $exec_array = get_object_vars($modele);
        $column = array_keys($exec_array);


        foreach($column as $key => $value){
            if($value == 'id'){
                unset($column[$key]);
            }else
            {
                $column[$key] = $value.'=:'.$value;
            }
        }
        $id= $modele->id;
        $req_prep="UPDATE $tableName  SET ".implode(', ',$column)." WHERE id= :id ;" ;        
        $sth =$this->db_cnx->prepare($req_prep);
        $sth->execute($exec_array);
        $erreurs= $sth->errorInfo();
        if($erreurs[0] != "00000"){

        }
    }
    public function deleteById(int $id):bool
    {
        $query = sprintf(
            'DELETE FROM %s WHERE id= :id',
            $this->getTable()
        );
        $sth= $this->db_cnx->prepare( $query );
        $sth->execute(
            array(
                'id'=>$id
            )
        ); 

        $erreurs= $sth->errorInfo();

        if($erreurs[0] != "00000"){

            print_r($sth->errorInfo());
            return false;
        }
        return true;
    }
}
