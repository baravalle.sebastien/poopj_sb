<?php
namespace Core;

class Route
{
	public $uri;
	public string $controller;
	public string $action;
	public ?array $arg;

	public function __construct( $uri, string $controller, string $action, ?array $arg=null )
	{
		$this->action = $action;
		$this->controller = $controller;
		$this->uri = $uri;
		$this->arg = $arg;

	}
}