<?php

namespace Core;

/*
**	**	**	***	**	**	**	**	**	**	
**	AJOUT AU ROUTER					**
**	**	**	**	**	**	**	*	**	**
ENREGISTRER UNE ROUTE:
Le premier parametre est soit une STRING:
	au quel cas la route est enregistrée si elle
	est egale au chemin de l'uri
soit une CONDITION:
	exemple: $isConnected;
	au quel cas la route est enregistrée si vrai

ENREGISTRE UNE ROUTE PREND UN 4°eme element 
facultatif, un array pour paramettrer les methodes;

*/

class Router
{
	private static ?self $instance = null;

	// Liste des routes géré par l'application
	private array $_routes = [];

	public string $controllers_namespace = '\\';

	public static function get(): self
	{
		if( is_null( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * Déclare une route
	 *
	 * @param string $uri URI de la route
	 * @param string $controller Nom du controller à utiliser
	 * @param string $action Nom de l'action (méthode du controller) à exécuter
	 */
	public function registerRoute(  $uri, string $controller, string $action, ?array $args= null ): void
	{
		// enregistre ou non 
		$request_uri = $_SERVER[ 'REQUEST_URI' ];

		if(is_string($uri) ){

			if( $uri == $request_uri ) {
				$route = new Route( $uri, $controller, $action, $args );
				$this->_routes[] = $route;
			
			}

		}
		if(is_bool($uri) ){

			if( $uri ) {
				
				$route = new Route( $uri, $controller, $action, $args );
				$this->_routes[] = $route;
			
			}

		}

	}

	/**
	 * Démarrage du routeur
	 */
	public function start(): void
	{   

		// URL demandée
        $request_uri = $_SERVER[ 'REQUEST_URI' ];

		$i= 0;
		// Parcours des routes à la recherche de l'URL demandée
		foreach( $this->_routes as $route ) {
			$i ++;
			// echo"<div class='router-detail'><br>".$route->controller."  ".$route->action."   n° ".$i."</div>";
            	
			$controller_name = $this->controllers_namespace . '\\' . $route->controller;
			$action = $route->action;
			$arg = $route->arg;
			$controller = new $controller_name();

// ternaire => (is_array($arg)) ? $conttroller->$action($arg) : $controller->$action();

			if(is_array($arg)){
				$controller->$action($arg);

			}else{

				$controller->$action();
			}
            // break;
		}
	}
	public function purgeRoute():void{
		$this->_routes = [];
	}

	private function __construct() {}
	private function __clone() {}
	private function __wakeup()	{}
}
