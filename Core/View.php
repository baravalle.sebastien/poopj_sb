<?php

namespace Core;

class View
{
	private const VIEW_PATH = ROOT_PATH . 'App' . DS . 'Views' . DS;

	private string $_path_file;

	public function __construct( string $view_name )
	{
		$this->_path_file = self::VIEW_PATH . $view_name . '.php';

	}

	public function render( ?array $view_data = [] ): void
	{
		if($view_data){

			extract( $view_data );
		}

		require_once $this->_path_file;
	}
}
