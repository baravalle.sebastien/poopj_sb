<?php
/*
    appel de namespaces;
        I.Core
        II.APP
            Modele
            Repository
*/
use Core\Router;

use App\Models\User;
use App\Models\Chambre;
use App\Models\Reservation;
use App\Models\Adresse;

use App\Repositories\EquipementRepository;
use App\Repositories\ChambreRepository;
use App\Repositories\RepositoryManager;

define( 'DS', DIRECTORY_SEPARATOR );
define( 'ROOT_PATH', dirname(__FILE__) . DS );

// FONCTION NAMESPACES(linux)
set_include_path( './classes/' . PATH_SEPARATOR . get_include_path() );
spl_autoload_extensions( '.php , .class.php' );
spl_autoload_register();
function linux_namespaces_autoload ( $class_name )
    {
        /* use if you need to lowercase first char *
        $class_name  =  implode( DIRECTORY_SEPARATOR , array_map( 'lcfirst' , explode( '\\' , $class_name ) ) );/* else just use the following : */
        $class_name  =  implode( DIRECTORY_SEPARATOR , explode( '\\' , $class_name ) );
        static $extensions  =  array();
        if ( empty($extensions ) )
            {
                $extensions  =  array_map( 'trim' , explode( ',' , spl_autoload_extensions() ) );
            }
        static $include_paths  =  array();
        if ( empty( $include_paths ) )
            {
                $include_paths  =  explode( PATH_SEPARATOR , get_include_path() );
            }
        foreach ( $include_paths as $path )
            {
                $path .=  ( DIRECTORY_SEPARATOR !== $path[ strlen( $path ) - 1 ] ) ? DIRECTORY_SEPARATOR : '';
                foreach ( $extensions as $extension )
                    {
                        $file  =  $path . $class_name . $extension;
                        if ( file_exists( $file ) && is_readable( $file ) )
                            {
                                require $file;
                                return;
                            }
                    }
            }
        throw new Exception( _( 'class ' . $class_name . ' could not be found.' ) );
    }
spl_autoload_register( 'linux_namespaces_autoload' , TRUE , FALSE );

define('DB_HOST','localhost');
define('DB_USER','root');
define('DB_PASS','');
define('DB_NAME','air_bnb_sb');

define('ROOT_', '/TD_sb_POO/');
define('MYSITE_URI','http://localhost' );


/***    *** *** *** *** *** *** *** *** *** *** *** *** *** *** 
 *                                                          ***
***        MEGA FONCTION DE ROUTAGE : ROUTE APPLY           ***
***                                                         ***
*** *** *** *** *** *** *** *** *** *** *** *** *** *** *** ***
route apply execute les chemins puis return ou en dernier 
ressort renvoi sur page d'accueil si 
mauvais chemin
*/
session_start();

function routeApply($initiate = null){
    
    $router = Router::get();
    $router->controllers_namespace = '\App\Controllers';
    
/*  **  **  **  **  **  **  **  **  **  **  **
**  ETAPE 1. CONNEXION D'UTILISATEURS       **
**  **  **  **  **  **  **  **  **  **  **  **

*   I.Envoyer sur page de connexion:
        A-lors de l'arrivé sur le site
        B-sauf si déja connecté 
        
*/
    //  I.
$isConnected = isset($_SESSION['isConnected']);
    //  A-
    if(!$isConnected){
        $cssTab = [
            '<link rel="stylesheet" href="css/style.css">'
        ];
        $argsHead = [
            'title_head'	=> 'airTDbnb - Connexion',
            'cssTab'=>$cssTab
        ];
        $router->registerRoute( ROOT_, 'ConnexionController', 'head', $argsHead );
        $router->registerRoute( ROOT_, 'ConnexionController', 'body' );
        /*
        *   II. TRAITER RETOUR FORMULAIRE PAGE CONNEXION
        A-  INSCRIPTION
        B- CONNEXION
        C- DECONNEXION
        */
        //  A-
        $router->registerRoute( ROOT_.'index.php?action=inscription', 'ConnexionController', 'addUser' );
        
        //  B-
        $router->registerRoute( ROOT_.'index.php?action=connexion', 'ConnexionController', 'checkUser' );
        $router->start();
        $router->purgeRoute();
        $isConnected = isset($_SESSION['isConnected']);
    }
    
    // LA CONDITION D'UTILISATEUR CONNECTEE EST BONNE:
    //  ACCES AU SITE ET A SES FONCTIONNALITÉES    
    if($isConnected){

        /*
        **  **  **  **  **  **  **  **  **  **  
        **  RECHERCHE                       **
        **  **  **  **  **  **  **  **  **  **
        */
        $onSearch = $_SERVER[ 'REQUEST_URI' ] == ROOT_.'index.php?search';
        if($onSearch){                
            $argsHead = ['title_head'	=> 'airTDbnb - Recherche',];

            $router->registerRoute( true, 'ReservationController', 'head',$argsHead );
            $argsMenu = ['user_name'	=> $_SESSION['isConnected']->login,'user_role'	=> $_SESSION['isConnected']->role,        ];
            
            $router->registerRoute( true, 'ReservationController', 'ShowMenu',$argsMenu );
            $searchArg = [
                // LA LISTE D'ARGUMENT DONNE LE REPO ET
                // LA REQUETTE A Y FAIRE
                'ctrl'	=> 'App\Controllers\SearchController',
                'request'	=> 'search',
                'arg'=> $_POST['search_field'], 

            ];



            $tab_obj = $router->registerRoute(true, 'SearchController', 'List',$searchArg );
            $router->start();

            
            //appeller la liste
            $arg= ['list'=>$tab_obj];

            $router->registerRoute( $isConnected, 'BasicHtml', 'List', $arg );

            $router->start();


            return true;
        }


    //  DECONNEXION: redirige sur accueil et kill session)  
    $router->registerRoute( ROOT_.'index.php?deconnect', 'ConnexionController', 'deconnect' );
    

        /*
        *   *   *   *   *   *   *   *   *   *   *   *   *  
            CONDITION D'ACCES AUX PAGES
        *   *   *   *   *   *   *   *   *   *   *   *   *   
        */    
            // PAGES ANNONCEUR
            $isAnnonceur = $_SESSION['isConnected']->role == 'annonceur';
            // PAGES CLIENT
            $isClient = $_SESSION['isConnected']->role == 'client';
        if($isAnnonceur){
            /**
             ** **  **  **  **  **  **  **  **  **  **
             **     PAGES ANNONCEUR                 **
             ** **  **  **  **  **  **  **  **  **  ** 
            *       CONDITION D'ACCES
            *   I.  PAGE POUR CREER ANNONCE
            *   II. PAGE POUR VOIR RESERVATION
            *   III. PAGE POUR VOIR ANNONCES CRÉES
            */
                // CONDITIONS D'ACCES
                    // pour acceder a route de creation d'annonces url doit venir du formulaire: "addannonces":
                $onAddAnnonces = $_SERVER[ 'REQUEST_URI' ] == ROOT_.'index.php?addannonces';
                    // manage post indique qu'il faut router ver la reception de données de form
                $managePostAnnonce = isset($_POST['send_post_annonces']);
                    // see my annonces indique que lien pour voir annonce a été cliqué
                $onSeeMyAnnonces =   $_SERVER[ 'REQUEST_URI' ] == ROOT_.'index.php?myannonces';    
                $onSeeMyReservations =   $_SERVER[ 'REQUEST_URI' ] == ROOT_.'index.php?myreservations';    

                if($onAddAnnonces)
            {
            /*
            **  **  **  **  **  **  **  **  **  **  **  
            **  I. CREATION D'ANNONCE         **
            **  **  **  **  **  **  **  **  **  **  **
            */ 
                $argsHead = [
                    'title_head'	=> 'airTDbnb - Poster une Annonce',
                ];
    
                $router->registerRoute( true, 'AnnoncesController', 'head',$argsHead );
                $argsMenu = [
                    'user_name'	=> $_SESSION['isConnected']->login,
                    'user_role'	=> $_SESSION['isConnected']->role,
                    // LE PARAMETRE PAGE AMENAGE LE MENU SELON LES BESOIN DE LA PAGE
                    'page'=>'addAnnonces',
        
                ];
        
                $router->registerRoute( true, 'AnnoncesController', 'ShowMenu',$argsMenu );
                $router->registerRoute( true, 'AnnoncesController', 'body');
    
                $router->registerRoute( $managePostAnnonce, 'AnnoncesController', 'addAnnonce');
    
                $router->start();
                return true;
    
            }
            
            else if ($onSeeMyReservations)
            {
            /*
            **  **  **  **  **  **  **  **  **  **  **  
            **  II. VOIR LES RESERVATIONS           **
            **  **  **  **  **  **  **  **  **  **  **
            */ 
                $argsHead = [
                    'title_head'	=> 'airTDbnb - Mes Reservations',
                ];

                $router->registerRoute( true, 'ReservationController', 'head',$argsHead );
                $argsMenu = [
                    'user_name'	=> $_SESSION['isConnected']->login,
                    'user_role'	=> $_SESSION['isConnected']->role,
                    'page'=>'myReservations',
        
                ];
                $router->registerRoute( true, 'ReservationController', 'ShowMenu',$argsMenu );
                $argReservationByUserId=[
                    'annonceur_id'=>$_SESSION['isConnected']->id
                ];
                $argsList = [
                    'repo'	=> 'getReservationRepo',
                    'request'	=> 'reservationByUser',
                    'arg'=> $argReservationByUserId,    
                ];

                $router->registerRoute( true, 'ReservationController', 'List', $argsList );

                $router->start();
            return true;
                return true;
            } 
            /*
            **  **  **  **  **  **  **  **  **  **  **  **
            **  DERNIERE ETAPE ACCUEIL                  **
            **  III.  AFFICHAGE DE SES ANNONCES         **
            **  **  **  **  **  **  **  **  **  **  **  **  
            */
                $argsHead = [
                    'title_head'	=> 'airTDbnb - Mes Annonces',
                ];
                $router->registerRoute( $isConnected, 'BasicHtml', 'head', $argsHead );
                $argsMenu = [
                    'user_name'	=> $_SESSION['isConnected']->login,
                    'user_role'	=> $_SESSION['isConnected']->role, 
                    'page'=>'myAnnonces',
                ];
    
                $router->registerRoute($isConnected, 'BasicHtml', 'showMenu', $argsMenu );
                $argsList = [
                    // LA LISTE D'ARGUMENT DONNE LE REPO ET
                    // LA REQUETTE A Y FAIRE
                    'repo'	=> 'getAllRoomInfoRepo',
                    'request'	=> 'getAllRoomInfo',
                    'arg'=> $_SESSION['isConnected']->id,
    
                    'user_name'	=> $_SESSION['isConnected']->login,
                    'user_role'	=> $_SESSION['isConnected']->role,    
                ];
    
                $router->registerRoute( $isConnected, 'BasicHtml', 'List', $argsList );
                $router->start();
                return true;
        }
        else if($isClient)
        {
            /**
             ** **  **  **  **  **  **  **  **  **  **
             **     PAGES CLIENT                    **
             ** **  **  **  **  **  **  **  **  **  ** 
            *       CONDITION D'ACCES
            *   I.  CLICK SUR RESERVER 
            *   II. PAGE POUR VOIR MES RESERVATIONS
            *   III. PAGE POUR VOIR RESERVATION
            */
            // CONDITIONS D'ACCES
            // pour acceder a route de Reservation url doit venir du formulaire: "addannonces":
                $onAddReservation = $_SERVER[ 'REQUEST_URI' ] == ROOT_.'index.php?addreservation';
                $managePostReservation = isset($_POST['send_post_reserver']);
                $onSeeMyReservations =   $_SERVER[ 'REQUEST_URI' ] == ROOT_.'index.php?myreservations';    

            if($onAddReservation)
            {
            /*
            **  **  **  **  **  **  **  **  **  **  **  
            **  I. CREATION D'Reservation           **
            **  **  **  **  **  **  **  **  **  **  **
            *       1. AFFICHER FORM POUR PRENDRE DONNES SUPLEMENTAIRES:
            *       
            */ 
                $argsHead = [
                    'title_head'	=> 'airTDbnb - RESERVER',
                ];

                $router->registerRoute( true, 'ReservationController', 'head',$argsHead );
                $argsMenu = [
                    'user_name'	=> $_SESSION['isConnected']->login,
                    'user_role'	=> $_SESSION['isConnected']->role,
                    // LE PARAMETRE PAGE AMENAGE LE MENU SELON LES BESOIN DE LA PAGE
                    'page'=>'addReservation',
        
                ];
        
                $router->registerRoute( true, 'ReservationController', 'ShowMenu',$argsMenu );
                $router->registerRoute( true, 'ReservationController', 'body');

                $argsReservation = [
                    'chambre_id'	=> $_POST['add_reservation'],
                    'user_id'	=> $_SESSION['isConnected']->id,
                ];
                $router->registerRoute(  $managePostReservation, 'ReservationController', 'reserver',$argsReservation );
                $router->start();
                return true;

            }
            else if ($onSeeMyReservations){
            /*
            **  **  **  **  **  **  **  **  **  **  **  
            **  I. VOIR MES RESERVATIONS            **
            **  **  **  **  **  **  **  **  **  **  **
            *       
            *       
            */ 
                $argsHead = [
                    'title_head'	=> 'airTDbnb - Mes Reservations',
                ];

                $router->registerRoute( true, 'ReservationController', 'head',$argsHead );
                $argsMenu = [
                    'user_name'	=> $_SESSION['isConnected']->login,
                    'user_role'	=> $_SESSION['isConnected']->role,
                    // LE PARAMETRE PAGE AMENAGE LE MENU SELON LES BESOIN DE LA PAGE
                    'page'=>'myReservations',
        
                ];
                $router->registerRoute( true, 'ReservationController', 'ShowMenu',$argsMenu );
                $argReservationByUserId=[
                    'client_id'=>$_SESSION['isConnected']->id
                ];
                $argsList = [
                    // LA LISTE D'ARGUMENT DONNE LE REPO ET
                    // LA REQUETTE A Y FAIRE
                    'repo'	=> 'getReservationRepo',
                    'request'	=> 'reservationByUser',
                    'arg'=> $argReservationByUserId,    
                ];
    
                $router->registerRoute( true, 'ReservationController', 'List', $argsList );
    
    
                $router->start();
                return true;

            } 
            /*
            **  **  **  **  **  **  **  **  **  **  **  **  **    
            **  DERNIERE ETAPE. AFFICHAGE DE L'ACCUEIL      **
            **          VOIR TOUTES LES ANNONCES            **
            **  **  **  **  **  **  **  **  **  **  **  **  **
            */
                $argsHead = [
                    'title_head'	=> 'airTDbnb - Bienvenue',
                ];
                $router->registerRoute( $isConnected, 'BasicHtml', 'head', $argsHead );
    
                $argsMenu = [
                    'user_name'	=> $_SESSION['isConnected']->login,
                    'user_role'	=> $_SESSION['isConnected']->role,    
                ];
    
                $router->registerRoute($isConnected, 'BasicHtml', 'showMenu', $argsMenu );
                
                $argsList = [
                    // LA LISTE D'ARGUMENT DONNE LE REPO ET
                    // LA REQUETTE A Y FAIRE
                    'repo'	=> 'getAllRoomInfoRepo',
                    'request'	=> 'getAllRoomInfo',
    
                    'user_name'	=> $_SESSION['isConnected']->login,
                    'user_role'	=> $_SESSION['isConnected']->role,    
                ];
    
                $router->registerRoute( $isConnected, 'BasicHtml', 'List', $argsList );
                $router->start();
                return true;
        }    
    }
    // VERIFIE QUE LA PAGE AI EU UN RESULTAT "return true", sinon REINITIALISE 
    if($initiate === 'do'){
        $router->registerRoute( true, 'ConnexionController', 'head', $argsHead );
        
        $router->registerRoute( true, 'ConnexionController', 'body' );
        $router->start();
    }
}
$wellAppend = routeApply();
if (!$wellAppend){
    routeApply('do');
}