DROP DATABASE IF EXISTS air_bnb_sb;
CREATE DATABASE air_bnb_sb CHARACTER SET 'utf8';
USE air_bnb_sb;
CREATE TABLE users (
    id INT(10) UNSIGNED UNIQUE NOT NULL AUTO_INCREMENT PRIMARY KEY,
    login VARCHAR(50) NOT NULL UNIQUE ,
    password VARCHAR(255) NOT NULL,
    role ENUM('annonceur','client') NOT NULL
);
CREATE TABLE chambres (
    
    id INT(10) UNSIGNED UNIQUE NOT NULL AUTO_INCREMENT PRIMARY KEY,
    label VARCHAR(50) NOT NULL UNIQUE  ,
    annonceur_id INT(10) UNSIGNED NOT NULL,
    prix_n INT(10) NOT NULL,
    type INT(10) NOT NULL,
    description VARCHAR(270) NOT NULL  ,
    couchage INT(10) NOT NULL,
    taille INT,
    image VARCHAR(250),
    date_debut DATETIME ,
    date_fin DATETIME,
    CONSTRAINT fk_chambres_annonceur_id         
        FOREIGN KEY (annonceur_id)             
        REFERENCES users(id)        

)ENGINE=InnoDB;

   
CREATE TABLE reservations (
    
    id INT(10) UNSIGNED UNIQUE NOT NULL AUTO_INCREMENT PRIMARY KEY,
    chambre_id INT(10) UNSIGNED NOT NULL,
    client_id INT(10) UNSIGNED NOT NULL,
    date_debut DATETIME NOT NULL,
    date_fin DATETIME NOT NULL,
    prix_t INT(10) NOT NULL,
    CONSTRAINT fk_reservation_chambre_id        
        FOREIGN KEY (chambre_id)             
        REFERENCES chambres(id) ,       
    CONSTRAINT fk_reservation_client_id        
        FOREIGN KEY (client_id)             
        REFERENCES users(id)        


)ENGINE=InnoDB;
CREATE TABLE adresses (
    
    id INT(10) UNSIGNED UNIQUE NOT NULL AUTO_INCREMENT PRIMARY KEY,
    chambre_id INT(10) UNSIGNED NOT NULL,
    pays VARCHAR(50) NOT NULL,
    code_postal INT(10) NOT NULL,
    ville VARCHAR(50) NOT NULL,
    voie VARCHAR(150) NOT NULL,
    numero INT(10) ,
    CONSTRAINT fk_adresse_chambre_id        
        FOREIGN KEY (chambre_id)             
        REFERENCES chambres(id)        
)ENGINE=InnoDB;
CREATE TABLE equipements (
  id INT(10) UNSIGNED UNIQUE NOT NULL AUTO_INCREMENT PRIMARY KEY,
  chambre_id INT(10) UNSIGNED,
  materiel VARCHAR(50),
    CONSTRAINT fk_materiel_chambre_id        
        FOREIGN KEY (chambre_id)             
        REFERENCES chambres(id)        


)ENGINE=InnoDB;




INSERT INTO users (login,password, role) 
VALUES  ('admin','8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 'annonceur');



